using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonManager : MonoBehaviour
{
    static SingletonManager m_Instance;
    public static SingletonManager Instance { get { return m_Instance; } }
    public GameManager GameManager;
    public LevelManager LevelManager;
    public HUDManager HUDManager;
    public MenuManager MenuManager;

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
            Destroy(gameObject);
    }
}
