using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private bool dirrection;
    [SerializeField] private bool close;
    [SerializeField] private bool animating;
    [SerializeField] private AnimationCurve doorCurve;
    [SerializeField] private Transform door;

    [SerializeField] private float baseRotationY;
    [SerializeField] private float speed = 1f;
    [SerializeField] private float angle = 75f;

    private float t;
    // Start is called before the first frame update
    void Start()
    {
        baseRotationY = door.transform.rotation.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (!animating) return;
        t += (close ? -Time.deltaTime : Time.deltaTime) * speed;
        if (t>1 || t < 0)
        {
            animating = false;
        }

        door.transform.rotation = Quaternion.Euler(new Vector3(door.transform.rotation.eulerAngles.x, Mathf.LerpUnclamped(baseRotationY, baseRotationY + (dirrection ? angle : -angle), doorCurve.Evaluate(t)), door.transform.rotation.eulerAngles.z));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            dirrection =  Vector3.Dot(other.transform.position - transform.position, transform.forward) > 0;
            // Debug.Log(Vector3.Dot(other.transform.position - transform.position, transform.forward));
            animating = true;
            close = false;
            t = Mathf.Clamp01(t);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            close = true;
            animating = true;
        }
    }
}
