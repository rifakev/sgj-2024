using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Object : MonoBehaviour, ObjectsInterface
{
    public int m_Password;
    public bool m_Tresor;
    public MeshFilter outlineMesh;
    public Material outlineFallback;

    private Material _objectMat;


    private MeshRenderer _objectRenderer;



    public void addPassword()
    {
        GameManager gameManager = SingletonManager.Instance.GameManager;
        HUDManager hudManager = SingletonManager.Instance.HUDManager;
        if (m_Tresor)
        {
            gameManager.getFinalPassword();
            Debug.Log("Mot de passe compilé");
        }
        else
        {
            gameManager.m_PassObtained++;
            gameManager.m_PasswordsList.Add(m_Password);
            hudManager.DisplayPasswordList();
            Destroy(gameObject);
        }
        
    }

    public void GetObjectType(out int i)
    {
        i = m_Tresor ? 2 : 1;
        //throw new System.NotImplementedException();
    }

    public void SetOutlineStatus(bool on)
    {
        if (outlineMesh == null && _objectRenderer != null)
        {
            _objectRenderer.material = on ? outlineFallback : _objectMat;
        }
        else if (outlineMesh != null)
        {
            outlineMesh.gameObject.SetActive(on);
        }
        
    }

    private void Start()
    {
        _objectRenderer = GetComponent<MeshRenderer>();

        if (outlineMesh == null)
        {
            _objectMat = _objectRenderer.material;
            return;
        }
        outlineMesh.gameObject.SetActive(false);
    }
}
