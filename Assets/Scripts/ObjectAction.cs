using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class ObjectAction : MonoBehaviour, ObjectsInterface
{
    [SerializeField] private AnimationCurve animCurve;
    [SerializeField] private float animationSpeed = 4f;
    [SerializeField] private float animationPower = -0.2f;
    [SerializeField] private bool useRotation;
    [SerializeField] private float pitchRandom = 0.2f;
    [SerializeField] private AudioClip[] audioClips;

    private AudioSource audioSource;

    private bool isAnimating;
    private bool oppened;
    private float t;
    private Vector3 basePosition;
    private float baseRotationY;

    public MeshFilter outlineMesh;
    // Start is called before the first frame update
    void Start()
    {
        basePosition = transform.position;
        baseRotationY = transform.rotation.eulerAngles.y;
        audioSource = GetComponent<AudioSource>();

        if (outlineMesh == null) return;
        outlineMesh.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //testing
        if (Input.GetKeyDown(KeyCode.A)){
            Debug.Log("anim");

            UseObject();
        }
        
        if (!isAnimating) return;
        t = t + (oppened ? -Time.deltaTime : Time.deltaTime) * animationSpeed;

        if (t < 1 && t > 0) UpdateObjectPosition();
        else
        {
            isAnimating = false;
            oppened = !oppened;
        }


    }

    public void UseObject()
    {
        audioSource.pitch = Random.Range(-pitchRandom, pitchRandom) + 1;
        audioSource.clip = audioClips[Random.Range(0, audioClips.Length - 1)];
        audioSource.Play();

        if (isAnimating)
        {
            oppened = !oppened;
            return;
        }

        t = oppened ? 1 : 0;

        isAnimating = true;

    }

    void UpdateObjectPosition()
    {
        //a se faire ovverride pour les autres la comportement de base

        if (useRotation) transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, Mathf.Lerp(baseRotationY, baseRotationY + animationPower, animCurve.Evaluate(t)), transform.rotation.eulerAngles.z));
        else transform.position = Vector3.Lerp(basePosition, (transform.forward * animationPower) + basePosition, animCurve.Evaluate(t));
    }

    public void SetOutlineStatus(bool on)
    {
        outlineMesh.gameObject.SetActive(on);
        //throw new System.NotImplementedException();
    }

    public void GetObjectType(out int i)
    {
        i = 2;
        //throw new System.NotImplementedException();
    }
}
