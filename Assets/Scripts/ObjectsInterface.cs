using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ObjectsInterface
{
    void SetOutlineStatus(bool on);
    void GetObjectType(out int i);
}
