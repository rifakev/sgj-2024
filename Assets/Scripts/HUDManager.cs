using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUDManager : MonoBehaviour
{
    public TextMeshProUGUI m_TimerText;
    public TextMeshProUGUI m_PassList;

    // MAJ de la liste des passwords
    public void DisplayPasswordList() {
        m_PassList.text = "";
        foreach (int password in SingletonManager.Instance.GameManager.m_PasswordsList)
        {
            m_PassList.text += password + "\n";
        }
    }

    public void DisplayTime(float time) {
        m_TimerText.text = time.ToString();
    }
}
