using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [Header("Caméra")]
    [SerializeField] Camera m_PlayerCamera;
    private float cameraRotationX = 0f;

    [Header("Vitesses")]
    public float m_MoveSpeed;
    public float m_RotateSpeed;
    
    float xInput, yInput;
    Vector2 rotation = Vector2.zero;
    float rotationX = 0;
    public float lookXLimit = 45.0f;

    private void FixedUpdate() 
    {
        MovePlayer();
    }

    void MovePlayer()
    {
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetAxis("Vertical");
        float moveSpeed = m_MoveSpeed * yInput;
        float rotateSpeed = m_RotateSpeed;

        if (yInput != 0) {
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        }

        // float mouseX = Input.GetAxis("Mouse X") * rotateSpeed;
        // float mouseY = Input.GetAxis("Mouse Y") * rotateSpeed;

        // // Rotation sur l'axe Y pour le GameObject (left/right)
        // transform.Rotate(Vector3.up * mouseX);

        // // Rotation sur l'axe X pour la caméra (up/down)
        // m_PlayerCamera.transform.Rotate(Vector3.right * -mouseY);

        // // Limiter la rotation de la caméra entre -90 et 90 degrés
        // cameraRotationX -= mouseY;
        // cameraRotationX = Mathf.Clamp(cameraRotationX, -90f, 90f);

        rotationX += -Input.GetAxis("Mouse Y") * m_RotateSpeed;
        rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
        m_PlayerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * m_RotateSpeed, 0);
    }

    float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;
        return Mathf.Clamp(angle, min, max);
    }
}
