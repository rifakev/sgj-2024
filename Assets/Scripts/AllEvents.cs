using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

#region GameManager Events
public class GameMenuEvent : SDD.Events.Event
{
}

public class GameInitLevelEvent : SDD.Events.Event
{
}

public class GamePlayEvent : SDD.Events.Event
{
    public int time { get; set; }
}

public class GameTutoEvent : SDD.Events.Event
{
}

public class GamePauseEvent : SDD.Events.Event
{
}

public class GameResumeEvent : SDD.Events.Event
{
}

public class GameVictoryEvent : SDD.Events.Event
{
}

public class GameOverEvent : SDD.Events.Event
{
}

public class GameStatisticsEvent : SDD.Events.Event
{
    public int eLifePoints { get; set; }
    public int eEnergyPoints { get; set; }
    public int eStaminaPoints { get; set; }
}
#endregion

#region MenuManager Events

public class GameCreditsEvent : SDD.Events.Event
{
}
public class PlayButtonClickedEvent : SDD.Events.Event
{
}
public class TutoButtonClickedEvent : SDD.Events.Event
{
}
public class MainMenuButtonClickedEvent : SDD.Events.Event
{
}
public class CreditsButtonsClickedEvent : SDD.Events.Event
{
}
public class PauseButtonClickedEvent : SDD.Events.Event
{
}
public class ResumeButtonClickedEvent : SDD.Events.Event
{
}
public class EnablePlayerMovement : SDD.Events.Event
{
}

public class DisablePlayerMovement : SDD.Events.Event
{
}

#endregion