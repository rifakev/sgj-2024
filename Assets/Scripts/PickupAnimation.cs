using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupAnimation : MonoBehaviour
{
    [SerializeField] private float lerpSpeed = 1f;

    private Vector3 offset;
    [SerializeField] private Transform target;
    [SerializeField] private float distanceTOStartToShrink = 50f;
    private bool ready;
    private Vector3 baseScale;

    private void Start()
    {
        baseScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Setup(target, new Vector3(0, -2f, 0));
        }
        if (!ready) return;
        float delta = Time.deltaTime;
        transform.position = Vector3.Lerp(transform.position, target.position + offset, delta * lerpSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, delta * lerpSpeed);

        float distance = Vector3.Distance(transform.position, target.position + offset);

        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, delta*lerpSpeed);

        Debug.Log(distance);
        
        if (distance < 0.1f ) {
            Destroy(gameObject);
        }
    }

    public void Setup(Transform destination, Vector3 finaloffset) { 
        target = destination;
        offset = finaloffset;
        ready = true;
    }

    public static float map(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
