using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RaycastDetector : MonoBehaviour
{
    [SerializeField] private bool useForward;
    [SerializeField] private Transform cameraTransform;

    [SerializeField] private TMP_Text text;


    private ObjectsInterface _lastHovered;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (useForward)
            {
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, Mathf.Infinity))
                {
                    Debug.DrawRay(cameraTransform.position, cameraTransform.forward * hit.distance, Color.yellow);
                    Debug.Log("Did Hit");
                    Object clickableObject = hit.collider.GetComponent<Object>();
                    if (clickableObject != null)
                    {
                        clickableObject.addPassword();
                    }

                    ObjectAction objectAction = hit.collider.GetComponent<ObjectAction>();
                    if (objectAction != null)
                    {
                        objectAction.UseObject();
                    }
                }
                else
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                    Debug.Log("Did not Hit");
                }

            }

        }
        else
        {
            RaycastHit hit;
            if (useForward)
            {
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, Mathf.Infinity))
                {
                    ObjectsInterface tempinterface;
                    if (hit.collider.TryGetComponent<ObjectsInterface>(out tempinterface))
                    {
                        if (_lastHovered != tempinterface && _lastHovered != null)
                        {
                            _lastHovered.SetOutlineStatus(false);
                        }
                        tempinterface.SetOutlineStatus(true);
                        int i;
                        tempinterface.GetObjectType(out i);
                        SetTooltipText(i);
                        _lastHovered = tempinterface;
                    }
                    else
                    {
                        DeOutline();
                    }
                    
                }
                else
                {
                    DeOutline();
                }

            }
        }

        void DeOutline()
        {
            if (_lastHovered != null)
            {
                _lastHovered.SetOutlineStatus(false);
                _lastHovered = null;
                SetTooltipText(0);
            }

        }

        void SetTooltipText(int i)
        {
            switch (i)
            {
                case 2:
                    text.text = "Click gauche ouvrir";
                    break; 
                case 1:
                    text.text = "Click gauche récupérer";
                    break; 
                case 0:
                    text.text = "";
                    break;
            }
        }
    }
}
