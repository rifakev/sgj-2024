using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class StepSoundManager : MonoBehaviour
{
    private Vector3 lastSoundLocation;
    [SerializeField] private float distanceThreshold = 1.5f;
    [SerializeField] private float pitchRandom = 0.2f;
    [SerializeField] private float volumeBase = 0.384f;
    private AudioSource audioSource;
    [SerializeField] private AudioClip[] audioClips;
    // Start is called before the first frame update
    void Start()
    {
        lastSoundLocation = transform.position;
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = volumeBase;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(lastSoundLocation, transform.position) > distanceThreshold)
        {
            lastSoundLocation = transform.position;
            audioSource.pitch = Random.Range(-pitchRandom, pitchRandom) + 1;
            audioSource.clip = audioClips[Random.Range(0, audioClips.Length - 1)];
            audioSource.Play();
            Debug.Log("played");
        }
    }
}
