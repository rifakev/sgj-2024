using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    void OnTriggerEnter(Collider player)
    {
        if (player.CompareTag("Player"))
        {
            SingletonManager.Instance.GameManager.EndGame();
        }
    }
}
