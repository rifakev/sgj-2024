using System.Collections;
using System.Collections.Generic;
using SDD.Events;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject m_MenuPanel;
    [SerializeField] GameObject m_TutoPanel;
    [SerializeField] GameObject m_GamePanel;
    [SerializeField] GameObject m_PausePanel;
    [SerializeField] GameObject m_CreditsPanel;
    [SerializeField] GameObject m_GameOverPanel;
    [SerializeField] GameObject m_VictoryPanel;
    List<GameObject> m_AllPanels = new List<GameObject>();
    private bool isPaused;

    /* Init Events
    ----------------------------------------------------*/
    public void SubscribeEvents()
    {
        EventManager.Instance.AddListener<GameMenuEvent>(GameMenu);
        EventManager.Instance.AddListener<GameTutoEvent>(GameTutorial);
        EventManager.Instance.AddListener<GamePlayEvent>(GamePlay);
        EventManager.Instance.AddListener<GameVictoryEvent>(GameVictory);
        EventManager.Instance.AddListener<GameOverEvent>(GameOver);
        EventManager.Instance.AddListener<GameCreditsEvent>(GameCredits);
        EventManager.Instance.AddListener<GamePauseEvent>(GamePause);
    }

    public void UnsubscribeEvents()
    {
        EventManager.Instance.RemoveListener<GameMenuEvent>(GameMenu);
        EventManager.Instance.RemoveListener<GameTutoEvent>(GameTutorial);
        EventManager.Instance.RemoveListener<GamePlayEvent>(GamePlay);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(GameVictory);
        EventManager.Instance.RemoveListener<GameOverEvent>(GameOver);
        EventManager.Instance.RemoveListener<GameCreditsEvent>(GameCredits);
        EventManager.Instance.RemoveListener<GamePauseEvent>(GamePause);
    }

    private void OnEnable()
    {
        SubscribeEvents();
    }

    private void OnDisable() 
    {
        UnsubscribeEvents();    
    }

    void Awake()
    {
        m_AllPanels.AddRange(new GameObject[]{
            m_MenuPanel, m_GamePanel, m_TutoPanel, m_PausePanel, m_CreditsPanel, m_GameOverPanel, m_VictoryPanel
        });
    }


    /* GameManager Events
    ----------------------------------------------------*/
    void DisplayPanel(GameObject panelGO)
    {
        foreach (var item in m_AllPanels)
            item.SetActive(item == panelGO);
    }
    
    void GamePlay(GamePlayEvent e){
        DisplayPanel(m_GamePanel);
    }

    void GameTutorial(GameTutoEvent e){
        DisplayPanel(m_TutoPanel);
    }

    void GameMenu(GameMenuEvent e){
        DisplayPanel(m_MenuPanel);
    }

    void GamePause(GamePauseEvent e){
        DisplayPanel(m_PausePanel);
    }

    void GameVictory(GameVictoryEvent e){
        DisplayPanel(m_VictoryPanel);
    }

    void GameOver(GameOverEvent e){
        DisplayPanel(m_GameOverPanel);
    }

    void GameCredits(GameCreditsEvent e){
        DisplayPanel(m_CreditsPanel);
    }


    /* UI Events
    ----------------------------------------------------*/
    public void PlayButtonClicked(){
        EventManager.Instance.Raise(new PlayButtonClickedEvent());
    }
    public void TutoButtonClicked(){
        EventManager.Instance.Raise(new TutoButtonClickedEvent());
    }

    public void CreditsButtonClicked(){
        EventManager.Instance.Raise(new CreditsButtonsClickedEvent());
    }

    public void PauseButtonClicked(){
        EventManager.Instance.Raise(new PauseButtonClickedEvent());
    }

    public void MainMenuButtonClicked(){
        EventManager.Instance.Raise(new MainMenuButtonClickedEvent());
    }

    public void QuitButtonClicked(){
        Application.Quit();
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }


}
