using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SDD.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum GAMESTATE { menu, credits, tuto, initLevel, play, pause, resume, victory, gameover }

public class GameManager : MonoBehaviour
{
    static GameManager m_Instance;
    GAMESTATE m_State;
    public bool IsPlaying { get { return m_State == GAMESTATE.play; } }
    bool m_InGame = false;

    [Header("Configuration du Timer")]
    public float m_Timer;
    private float currTime;
    private bool stopTimer;

    [Header("Gestion des mots de passe")]
    public GameObject m_PasswordsListContent;
    public GameObject m_InputPasswordContent;
    public GameObject m_DoorFinal;
    public GameObject m_Cadenas;
    public List<int> m_PasswordsList = new List<int>();
    public int m_PassCount = 3;
    public int m_PassObtained;
    private int m_passResult;
    public TMP_InputField m_InputPassword;
    [SerializeField] private TMP_Text wrongPassText;

    public void SubscribeEvents()
    {
        EventManager.Instance.AddListener<PlayButtonClickedEvent>(PlayButtonClicked);
        EventManager.Instance.AddListener<TutoButtonClickedEvent>(TutoButtonClicked);
        EventManager.Instance.AddListener<MainMenuButtonClickedEvent>(MainMenuButtonClicked);
        EventManager.Instance.AddListener<CreditsButtonsClickedEvent>(CreditsButtonClicked);
    }
    public void UnsubscribeEvents()
    {
        EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(PlayButtonClicked);
        EventManager.Instance.RemoveListener<TutoButtonClickedEvent>(TutoButtonClicked);
        EventManager.Instance.RemoveListener<MainMenuButtonClickedEvent>(MainMenuButtonClicked);
        EventManager.Instance.RemoveListener<CreditsButtonsClickedEvent>(CreditsButtonClicked);
    }

    void OnEnable()
    {
        SubscribeEvents();
    }

    void OnDisable()
    {
        UnsubscribeEvents();
    }


    /* GAME MANAGE
    ----------------------------------------------------*/
    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        Menu();
        currTime = m_Timer;
        Time.timeScale = 0;
        m_InputPassword.onEndEdit.AddListener(delegate { compareFinalPassword(); });
    }

    private void Update() {
        HandleTimer();
        HandleGameActivation();
        HandlePauseMenu();
        TogglePassList();
    }


    /* HANDLES
    ----------------------------------------------------*/
    #region handles
    // Activation du jeu
    private void HandleGameActivation()
    {
        if (IsPlaying) {
            Time.timeScale = 1;
        } 
    }

    // Activation / Désactivation du menu Pause
    private void HandlePauseMenu()
    {
        if (Input.GetKeyDown("escape") && m_InGame)
            if (IsPlaying)
                Pause();
            else
                Play();
    }

    // Gestion du Timer
    void HandleTimer()
    {
        if (m_InGame && !stopTimer)
        {
            if (currTime > 0) {
                currTime -= Time.deltaTime;
                SingletonManager.Instance.HUDManager.DisplayTime(Mathf.Round(currTime));
            } else {
                GameOver();
            }    
        }
    }

    // Configuration du mot de passe final
    public void getFinalPassword()
    {
        if (m_PassObtained == m_PassCount)
        {
            m_passResult = m_PasswordsList.Sum() % 1000;
            m_InputPasswordContent.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Debug.Log(m_passResult);
            EventManager.Instance.Raise(new DisablePlayerMovement());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(WrongPass(3f));
        }
        
    }

    
    // Entrée du mot de passe final
    public void compareFinalPassword()
    {
        if (m_InputPassword.text == m_passResult.ToString())
        {
            Debug.Log("Bon mot de passe");
            m_DoorFinal.GetComponent<Collider>().enabled = true;
            m_Cadenas.GetComponent<Collider>().enabled = false;
            m_Cadenas.SetActive(false);
            m_InputPasswordContent.SetActive(false);
            // EndGame();
            Cursor.lockState = CursorLockMode.Locked;
            EventManager.Instance.Raise(new EnablePlayerMovement());

        }
        else
        {
            Debug.Log("Mauvais mot de passe");

        }
    }

    
    private IEnumerator WrongPass(float delay)
    {
        wrongPassText.gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
        wrongPassText.gameObject.SetActive(false);
    }

    // Affichage des mots de passe
    public void TogglePassList()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            // Vérifie si le GameObject cible est actif
            if (m_PasswordsListContent.activeSelf)
            {
                // Désactive le GameObject s'il est actif
                m_PasswordsListContent.SetActive(false);
            }
            else
            {
                // Active le GameObject s'il est désactivé
                m_PasswordsListContent.SetActive(true);
            }    
        }
    }

    public void EndGame()
    {
        if (!stopTimer)
        {
            stopTimer = !stopTimer;
            Victory();
        }
    }

    #endregion


    /* STATES
    ----------------------------------------------------*/
    #region States
    void setAndBroadcastState(GAMESTATE state)
    {
        m_State = state;
        switch (m_State)
        {
            case GAMESTATE.menu:
                EventManager.Instance.Raise(new GameMenuEvent());
                m_InGame = false;
                break;
            case GAMESTATE.credits:
                EventManager.Instance.Raise(new GameCreditsEvent());
                break;
            case GAMESTATE.tuto:
                EventManager.Instance.Raise(new GameTutoEvent());
                break;
            case GAMESTATE.initLevel:
                EventManager.Instance.Raise(new GameInitLevelEvent());
                break;
            case GAMESTATE.play:
                EventManager.Instance.Raise(new GamePlayEvent());
                Cursor.lockState = CursorLockMode.Locked;
                m_InGame = true;
                break;
            case GAMESTATE.pause:
                EventManager.Instance.Raise(new GamePauseEvent());
                break;
            case GAMESTATE.resume:
                EventManager.Instance.Raise(new GameResumeEvent());
                break;
            case GAMESTATE.victory:
                EventManager.Instance.Raise(new GameVictoryEvent());
                break;
            case GAMESTATE.gameover:
                EventManager.Instance.Raise(new GameOverEvent());
                break;
            default:
                break;
        }
    }

    void Menu()
    {
        setAndBroadcastState(GAMESTATE.menu);
    }

    void Credits()
    {
        setAndBroadcastState(GAMESTATE.credits);
    }

    void InitLevel()
    {
        setAndBroadcastState(GAMESTATE.initLevel);
    }

    void Tuto()
    {
        setAndBroadcastState(GAMESTATE.tuto);
    }

    void Play()
    {
        setAndBroadcastState(GAMESTATE.play);
    }

    void Pause()
    {
        setAndBroadcastState(GAMESTATE.pause);
        Time.timeScale = 0;
    }

    public void Victory()
    {
        setAndBroadcastState(GAMESTATE.victory);
    }

    public void GameOver()
    {
        setAndBroadcastState(GAMESTATE.gameover);
    }
    #endregion


    /* UI EVENTS
    ----------------------------------------------------*/
    #region UIEvents
    void PlayButtonClicked(PlayButtonClickedEvent e)
    {
        Play();
    }

    void TutoButtonClicked(TutoButtonClickedEvent e)
    {
        Tuto();
    }

    void MainMenuButtonClicked(MainMenuButtonClickedEvent e)
    {
        Menu();
    }

    void CreditsButtonClicked(CreditsButtonsClickedEvent e)
    {
        Credits();
    }

    void PauseButtonClicked(PauseButtonClickedEvent e)
    {
        Pause();
    }
    #endregion
}
